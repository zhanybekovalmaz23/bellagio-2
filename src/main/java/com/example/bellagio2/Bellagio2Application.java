package com.example.bellagio2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bellagio2Application {

	public static void main(String[] args) {
		SpringApplication.run(Bellagio2Application.class, args);
	}

}
